package com.example.alok.nearbysearch;

import java.io.Serializable;

/**
 * Created by alok on 1/1/17.
 */

public class ResultPerameterModal implements Serializable{

    private String name;

    private String address;

    private String distance;



    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getDistance ()
    {
        return distance;
    }
    public void setDistance (String distance)
    {
        this.distance = distance;
    }



}
