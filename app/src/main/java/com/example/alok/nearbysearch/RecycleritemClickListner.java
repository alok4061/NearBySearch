package com.example.alok.nearbysearch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class RecycleritemClickListner implements RecyclerView.OnItemTouchListener{

    private OnItemClickListener listener;
    GestureDetector mGestureDetector;

    public RecycleritemClickListner(Context applicationContext, OnItemClickListener onItemClickListener) {
        listener=onItemClickListener;
        mGestureDetector = new GestureDetector(applicationContext, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    public interface OnItemClickListener{
        public void onItemClick(View view, int position);
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View view = rv.findChildViewUnder(e.getX(),e.getY());
        if(view!=null && listener!=null && mGestureDetector.onTouchEvent(e)){
            listener.onItemClick(view,rv.getChildAdapterPosition(view));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {


    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
