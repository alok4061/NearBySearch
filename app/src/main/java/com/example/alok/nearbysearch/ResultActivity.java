package com.example.alok.nearbysearch;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RequestQueue requestQueue;
    ArrayList arrayList = new ArrayList();
    String latR,lngR,lat1,lng1;
    double lat,lng,latR2,lngR2, dist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        recyclerView = (RecyclerView) findViewById(R.id.resultRecycler);
        requestQueue = Volley.newRequestQueue(this);
        Bundle bundle =getIntent().getExtras();
        lat = bundle.getDouble("lat");
        lng =bundle.getDouble("lng");
        lat1 = Double.toString(lat);
        lng1 = Double.toString(lng);

//        Log.d("fdgd",lat1);


        String str = bundle.getString("name");
        switch (str){
            case "atm":
                Jsondata("atm",lat1,lng1);
                break;
            case "bank":
                Jsondata("bank",lat1,lng1);
                break;
            case "restaurant":
                Jsondata("restaurant",lat1,lng1);
                break;
            case "hospital":
                Jsondata("hospital",lat1,lng1);
                break;
        }


    }
    public void Jsondata(final String name,String lati,String lngi){
        MyProgressDialog.showDialog(this);

        String url ="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+lati+","+lngi+"&radius=500&type="+name+"&key=AIzaSyB9qKTQA1DEx5KPDhLcp_-0IewEAZxdbnI";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                MyProgressDialog.hideDialog();
                try {
                    JSONArray jsonArray = jsonObject.getJSONArray("results");
                    for (int i =0;i<jsonArray.length();i++){
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        String placename= jsonObject1.getString("name");
                        String placAdd = jsonObject1.getString("vicinity");
                        JSONObject jsonObject2 = jsonObject1.getJSONObject("geometry");
                        JSONObject jsonObject3 = jsonObject2.getJSONObject("location");
                         latR = jsonObject3.getString("lat");
                         lngR = jsonObject3.getString("lng");
                        latR2 = Double.parseDouble(latR);
                        lngR2 = Double.parseDouble(lngR);

//                        Toast.makeText(ResultActivity.this,""+placename,Toast.LENGTH_SHORT).show();
//                        Log.d("ffsf",add);
                        ResultPerameterModal modal = new ResultPerameterModal();
                        modal.setName(placename);
                        modal.setAddress(placAdd);

                        arrayList.add(modal);

                    }


                    layoutManager = new LinearLayoutManager(ResultActivity.this);
                    recyclerView.setLayoutManager(layoutManager);
                    ResultAdapter resultAdapter = new ResultAdapter(arrayList);
                    recyclerView.setAdapter(resultAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ResultActivity.this,""+e,Toast.LENGTH_SHORT).show();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Toast.makeText(ResultActivity.this,""+volleyError,Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonObjectRequest);
        //Calculating distance
//        double earthRadius = 3958.75;
//
//        double dLat = Math.toRadians(latR2-lat);
//        double dLng = Math.toRadians(lngR2-lng);
//        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
//                Math.cos(Math.toRadians(lat)) * Math.cos(Math.toRadians(latR2)) *
//                        Math.sin(dLng/2) * Math.sin(dLng/2);
//        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
//        double dist = earthRadius * c;
//        dist = Math.acos(dist);
//        dist = rad2deg(dist);
//        dist = dist * 60 * 1.1515;
        distance(latR2,lngR2,lat,lng);
        Toast.makeText(ResultActivity.this,""+dist,Toast.LENGTH_SHORT).show();

    }
    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
         dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        Log.d("km",""+dist);
        Toast.makeText(ResultActivity.this,""+dist,Toast.LENGTH_SHORT).show();
        return (dist);




    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }




}
