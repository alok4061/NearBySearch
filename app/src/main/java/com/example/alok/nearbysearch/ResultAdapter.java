package com.example.alok.nearbysearch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ViewHolder>{

    ArrayList arrayList;

    public ResultAdapter(ArrayList arrayList) {
        this.arrayList=arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_result_adapter,null);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ResultPerameterModal perameterModal = (ResultPerameterModal) arrayList.get(position);
        Log.d("name",perameterModal.getName());
        Log.d("address",perameterModal.getAddress());

        holder.nametv.setText(perameterModal.getName());
        holder.addtv.setText(perameterModal.getAddress());

    }
    @Override
    public int getItemCount() {
//        Log.d("sizearray",""+arrayList.size());
        return arrayList.size();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nametv,addtv,distancetv;
        public ViewHolder(View itemView) {
            super(itemView);
            nametv = (TextView) itemView.findViewById(R.id.nameplace);
            addtv = (TextView) itemView.findViewById(R.id.vicinityName);
            distancetv = (TextView) itemView.findViewById(R.id.DistanceName);
        }
    }
}

